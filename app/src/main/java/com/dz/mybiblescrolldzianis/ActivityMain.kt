package com.dz.mybiblescrolldzianis

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


private const val ITEMS_AMOUNT = 50
private const val MAX_LINES = 4
private const val TEXT_LINE = "simple line of text\n"

class ActivityMain: AppCompatActivity(){

    private val rvAdapter1 = Adapter()
    private val rvAdapter2 = Adapter()
    private val rvLayout1 = LinearLayoutManager(this)
    private val rvLayout2 = LinearLayoutManager(this)

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rvInit()
        rvPopulate()
        rvTuneScrolling()
    }

    private fun rvInit(){
        recycler_1.apply {
            layoutManager = rvLayout1
            adapter = rvAdapter1
        }
        recycler_2.apply {
            layoutManager = rvLayout2
            adapter = rvAdapter2
        }
    }

    private fun rvPopulate(){
        val rand = Random()
        val data1 = ArrayList<String>(ITEMS_AMOUNT)
        val data2 = ArrayList<String>(ITEMS_AMOUNT)
        repeat(ITEMS_AMOUNT){
            StringBuilder().apply {
                repeat(1 + rand.nextInt(MAX_LINES)){ append(TEXT_LINE) }
                deleteCharAt(length - 1)
                data1.add(toString())
            }
            StringBuilder().apply {
                repeat(1 + rand.nextInt(MAX_LINES)){ append(TEXT_LINE) }
                deleteCharAt(length - 1)
                data2.add(toString())
            }
        }
        rvAdapter1.setData(data1)
        rvAdapter2.setData(data2)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    private fun rvTuneScrolling(){
        recycler_1.addOnScrollListener(mOnScrollListener)
        recycler_2.addOnScrollListener(mOnScrollListener)
    }

    private val mOnScrollListener = object : RecyclerView.OnScrollListener(){

        // for preventing auto scroll while recycler is Dragging
        private var rv1allowed = true
        private var rv2allowed = true

        override fun onScrollStateChanged(rv: RecyclerView, newState: Int){
            when(newState){
                RecyclerView.SCROLL_STATE_DRAGGING -> {
                    when(rv){
                        recycler_1 -> rv1allowed = false
                        recycler_2 -> rv2allowed = false
                    }
                }
                RecyclerView.SCROLL_STATE_IDLE, RecyclerView.SCROLL_STATE_SETTLING -> {
                    when(rv){
                        recycler_1 -> rv1allowed = true
                        recycler_2 -> rv2allowed = true
                    }
                }
            }
        }

        override fun onScrolled(rv: RecyclerView, dx: Int, dy: Int){
            val rvLayout: LinearLayoutManager
            val rvLayoutSlave: LinearLayoutManager
            if(rv == recycler_1){
                if( ! rv2allowed)
                    return
                rvLayoutSlave = rvLayout2
                rvLayout = rvLayout1
            }else{//if(rv == recycler_2)
                if( ! rv1allowed)
                    return
                rvLayoutSlave = rvLayout1
                rvLayout = rvLayout2
            }

            val fPos = rvLayout.findFirstVisibleItemPosition()

            var slaveFirstView = rvLayoutSlave.findViewByPosition(fPos)
            if(slaveFirstView == null){
                /// we scroll here 1px down for to say to recycler to bind view for previous item
                rvLayoutSlave.scrollToPositionWithOffset(fPos + 1, 1)
                slaveFirstView = rvLayoutSlave.findViewByPosition(fPos)
            }
            slaveFirstView?.let { slaveView ->
                val view = rvLayout.findViewByPosition(fPos)
                val vK = view.top.toFloat() / view.height

                rvLayoutSlave.scrollToPositionWithOffset(fPos, (slaveView.height * vK).toInt())
            }
        }
    }

}
