package com.dz.mybiblescrolldzianis

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView


class Adapter: RecyclerView.Adapter<Adapter.VHolder>(){

    private var items: MutableList<String> = ArrayList(0)

    override fun getItemCount() = items.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
        = VHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_rv, parent, false))

    override fun onBindViewHolder(holder: VHolder, position: Int)
        = holder.bind(items[position], position)


    fun setData(list: MutableList<String>){
        items = list
        notifyDataSetChanged()
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////

    class VHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        private val tvText = itemView as TextView

        fun bind(item: String, position: Int){
            tvText.setBackgroundColor(if(position % 2 == 0) 0 else 0x11000000)
            tvText.text = item
        }
    }

}
